import { transformGoogleDriveLink } from "@/src/lib/transformGoogleDriveLink";
import Image from 'next/image';
import { useState } from "react";

export default function CardWithImage({ cardData }) {
  const [isDescriptionToggled, toggleDescription] = useState(false);

  const a = isDescriptionToggled ? " tabs__content-item-ext--open" : "";

  return (
    <div className={`tabs__content-item-ext${a}`}>
      <div className="tabs__content-item-ext-info">
        <div className="extended-info">
          <h2>{cardData.name}</h2>
          <p>{cardData.description}</p>
        </div>
        <div className="tabs__content-item-ext-info-info">
          <div>
            <span>Цена (руб)</span>
            <strong>{cardData.price}</strong>
          </div>

          {cardData.description && (
            <>
              <button
                className="toggleButton"
                onClick={() => toggleDescription(!isDescriptionToggled)}
              >
                <svg className="icon icon-info tabs__content-item-ext-btn tabs__content-item-ext-btn--info">
                  <use xlinkHref="img/sprite.svg#icon-info"></use>
                </svg>
                <svg className="icon icon-close tabs__content-item-ext-btn tabs__content-item-ext-btn--close">
                  <use xlinkHref="img/sprite.svg#icon-close"></use>
                </svg>
              </button>

              <div>
                {cardData.volume && (
                  <>
                    <span>Вес (г)</span>
                    <span> </span>
                    <strong>{cardData.volume}</strong>
                  </>
                )}
              </div>
            </>
          )}
        </div>
      </div>
      <div className="tabs__content-item-ext-img">
        <Image width={500} height={500} src={transformGoogleDriveLink(cardData.imageLink)} alt="" />
      </div>
    </div>
  );
}
