import { useEffect } from "react";
import CardPlain from "./CardPlain";
import CardWithImage from "./CardWithImage";

export default function ContentTabs({ contentName, contentData }) {
  if (!contentData) {
    return <></>
  }

  const sanitizeMenuItem = (string) => {
    if (string === 'default') {
      return null
    }

    return string.replaceAll("*", "")
  };

  const renderNestedContentData = (data) => {
    const keys = Object.keys(data)

    const renderNestedTitle = (key) => (
      data[key][0].imageLink
        ? <span className="btn btn--blur btn--on-tabs">{sanitizeMenuItem(key)}</span>
        : <span className="tabs__item-sub-title">{sanitizeMenuItem(key)}</span>
      )

    return keys.map(key => {
      return <>

        {key !== 'default' && renderNestedTitle(key)}

        {
          data[key].map(contentDataItem => {
            if (contentDataItem.imageLink) {
              return <CardWithImage cardData={contentDataItem} key={contentDataItem.name} />
            } else {
              return <CardPlain cardData={contentDataItem} key={contentDataItem.name} />
            }
          })
        }
      </>
    })
  }

  const isTopTitleNeedsToBeRendered = () => {
    if (Array.isArray(contentData)) {
      return true
    }

    const firstMenuItem = contentData[Object.keys(contentData)[0]][0]

    if (firstMenuItem.subtype && firstMenuItem.subtype !== 'default' && firstMenuItem.imageLink) {
      return false
    }

    return true
  }

  if (Array.isArray(contentData)) {
    return (
      <div className="tabs__wrapper">
        <div className="tabs__content tabs__content--current">
        { isTopTitleNeedsToBeRendered() && <span className="btn btn--blur btn--on-tabs">{sanitizeMenuItem(contentName)}</span>}
  
          {
            contentData.map(contentDataItem => {
              if (contentDataItem.imageLink) {
                return <CardWithImage cardData={contentDataItem} key={contentDataItem.name} />
              } else {
                return <CardPlain cardData={contentDataItem} key={contentDataItem.name} />
              }
            })
          }
        </div>
      </div>
    );
  } else {
    return (
      <div className="tabs__wrapper">
        <div className="tabs__content tabs__content--current">
          { isTopTitleNeedsToBeRendered() && <span className="btn btn--blur btn--on-tabs">{sanitizeMenuItem(contentName)}</span>}
  
          {renderNestedContentData(contentData)}
        </div>
      </div>
    );
  }
}
