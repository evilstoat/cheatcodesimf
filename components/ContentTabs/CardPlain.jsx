export default function CardPlain({ cardData }) {
  return (
    <div className="tabs__content-item">
      <div className="tabs__content-info">
        <strong>{ cardData.name }</strong>
        <span>{ cardData.description }</span>
      </div>
      <div className="tabs__content-price">
        <span>{ cardData.volume }</span>
        <strong>{ cardData.price }</strong>
      </div>
    </div>
  );
}
