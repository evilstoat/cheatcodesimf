import { usePathname } from "next/navigation";

const seasonalTypeDictionary = {
  'Летнее предложение': 'tabs__item--special-summer',
  'Осеннее предложение': 'tabs__item--special-autumn',
  'Зимнее предложение': 'tabs__item--special-winter',
  'Весеннее предложение': 'tabs__item--special-spring',
};

export default function TopMenu({
  iconsList,
  currentType,
  types,
  setCurrentType,
}) {
  const pathname = usePathname()

  const isSpecial = (menuItem) => {
    return Object.keys(seasonalTypeDictionary).includes(menuItem)
  }

  const getXlinkHref = (menuItem) => {
    if (isSpecial(menuItem)) {
      if (pathname === '/drinks') {
        return `img/sprite.svg#icon-special-drinks`;
      }

      if (pathname === '/food') {
        return `img/sprite.svg#icon-special-food`;
      }
    }

    const iconRecord = iconsList.find(({ name }) => name === menuItem);

    if (!iconRecord) {
      return "img/sprite.svg#icon-cold-snacks";
    }

    return `img/sprite.svg#icon-${iconRecord.icon}`;
  };

  const sanitizeMenuItem = (string) => adjustString(string.replaceAll("*", ""));

  const getClassNames = (menuItem) => {
    const specialClass = isSpecial(menuItem) ? ` ${seasonalTypeDictionary[menuItem]}` : "";

    const isCurrent = menuItem === currentType ? " tabs__item--current" : "";

    return `tabs__item${specialClass}${isCurrent}`;
  };

  function adjustString(inputString) {
    if (inputString.length >= 18) {
      return inputString.substring(0, 14) + ".";
    } else {
      return inputString;
    }
  }

  return (
    <>
      <div className="tabs">
        <ul className="tabs__list">
          {types.map((menuItem) => (
            <li
              onClick={() => setCurrentType(menuItem)}
              key={menuItem}
              className={getClassNames(menuItem)}
            >
              <div className="tabs__ico-wrap">
                <div className="tabs__ico">
                  <svg className="icon icon-cold-snacks ">
                    <use xlinkHref={getXlinkHref(menuItem)}></use>
                  </svg>
                </div>
              </div>
              <span>{sanitizeMenuItem(menuItem)}</span>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}
