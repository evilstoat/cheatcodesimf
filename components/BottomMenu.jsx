import Link from "next/link";
import { usePathname } from 'next/navigation';

export default function BottomMenu() {
  const currentPathname = usePathname()

  const getLinkClassNames = (pathname) => {
    const activeClass = currentPathname === pathname ? ' nav__link--current' : ''

    return `nav__link${activeClass}`
  }

  return (
    <>
      <header className="header">
        <nav className="nav">
          <Link prefetch={false} className={getLinkClassNames("/")} href="/">
            <svg className="icon icon-home ">
              <use xlinkHref="img/sprite.svg#icon-home"></use>
            </svg>
            <span>Главная</span>
          </Link>
          <Link prefetch={false} className={getLinkClassNames("/food")} href="/food">
            <svg className="icon icon-food ">
              <use xlinkHref="img/sprite.svg#icon-food"></use>
            </svg>
            <span>Еда</span>
          </Link>
          <Link prefetch={false} className={getLinkClassNames("/hookah")} href="/hookah">
            <svg className="icon icon-hookah ">
              <use xlinkHref="img/sprite.svg#icon-hookah"></use>
            </svg>
            <span>Кальяны</span>
          </Link>
          <Link prefetch={false} className={getLinkClassNames("/drinks")} href="/drinks">
            <svg className="icon icon-drink ">
              <use xlinkHref="img/sprite.svg#icon-drink"></use>
            </svg>
            <span>Бар</span>
          </Link>
          <Link prefetch={false} className={getLinkClassNames("/feedback")} href="/feedback">
            <svg className="icon icon-user ">
              <use xlinkHref="img/sprite.svg#icon-user"></use>
            </svg>
            <span>Фидбек</span>
          </Link>
        </nav>
      </header>
    </>
  );
}
