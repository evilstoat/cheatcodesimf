import BottomMenu from "@/components/BottomMenu";
import Head from "next/head";
import { usePathname } from "next/navigation";
import Router from 'next/router';
import NProgress from 'nprogress';
import "nprogress/nprogress.css";
import "../public/css/app.css";
import "../public/css/nprogress.css";

NProgress.configure({
  minimum: 0.3,
  easing: 'ease',
  speed: 800,
  showSpinner: false,
});

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());



export default function App({ Component, pageProps }) {
  const currentPathname = usePathname();

  const renderBottomMenu = () =>
    currentPathname === "/" ? null : <BottomMenu />;

  return (
    <>
      <Head>
        <meta name="theme-color" content="#111114" />
        <meta name="msapplication-navbutton-color" content="#111114" />
      </Head>
      {renderBottomMenu()}
      <Component {...pageProps} />
    </>
  );
}
