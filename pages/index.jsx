import Head from "next/head";
import Link from "next/link";
import { useState } from "react";

export default function Index() {
  const [isVideoPassed, setVideoPassed] = useState(false);

  // При старте видео детектим его окончание и обновляем стейт
  const setVideoPlayDetection = () => {
    setTimeout(() => {
      setVideoPassed(true);
    }, 2500);
  };

  return (
    <>
      <Head>
        <title>Главная</title>
      </Head>
      <main className="start-page">
        <video
          id="videoMain"
          preload="auto"
          nocontrols="true"
          onPlay={setVideoPlayDetection}
          autoPlay
          playsInline
          muted
          style={{ display: isVideoPassed ? "none" : "block" }}
        >
          <source src="/img/first-load3.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        <div
          id="contentMain"
          style={{ display: isVideoPassed ? "block" : "none" }}
        >
          <div className="start-page__wrap start-page--animated">
            <Link prefetch={false} className="logo link-effect" href="/">
              <img src="/img/logo.png" alt="" />
            </Link>
            <div className="blur-bg">
              <Link
                prefetch={false}
                className="btn btn--size-m btn--blur"
                href="/food"
              >
                <span>Начать</span>
              </Link>
              <img src="/img/logo.png" alt="" />
            </div>
            <div className="contacts__list">
              <div className="contacts__item">
                <div className="contacts__item-title">
                  <svg className="icon icon-time ">
                    <use xlinkHref="img/sprite.svg#icon-time"></use>
                  </svg>
                  <span>график работы</span>
                </div>
                <div className="contacts__item-info">
                  <p>12:00 – 02:00 ( вс – чт )</p>
                  <p>12:00 – 03:00 ( пт – сб )</p>
                </div>
              </div>
              <div className="contacts__item">
                <a
                  className="contacts__link link-effect"
                  href="tel:+79780010105"
                  target="_blank"
                >
                  <div>
                    <div className="contacts__item-title">
                      <svg className="icon icon-phone ">
                        <use xlinkHref="img/sprite.svg#icon-phone"></use>
                      </svg>
                      <span>Бронь столов</span>
                    </div>
                    <div className="contacts__item-info">
                      <p>+7 (978) 001 01 05</p>
                    </div>
                  </div>
                  <div className="contacts__link-arrow">
                    <span className="btn btn--circle">
                      <svg className="icon icon-arrow ">
                        <use xlinkHref="img/sprite.svg#icon-arrow"></use>
                      </svg>
                    </span>
                  </div>
                </a>
              </div>
              <div className="contacts__item">
                <a
                  className="contacts__link link-effect"
                  href="https://yandex.ru/maps/-/CDFlR434"
                  target="_blank"
                >
                  <div>
                    <div className="contacts__item-title">
                      <svg className="icon icon-geo ">
                        <use xlinkHref="img/sprite.svg#icon-geo"></use>
                      </svg>
                      <span>Адрес</span>
                    </div>
                    <div className="contacts__item-info">
                      <p>г. Симферополь ул. Набережная 75с</p>
                    </div>
                  </div>
                  <div className="contacts__link-arrow">
                    <span className="btn btn--circle">
                      <svg className="icon icon-arrow ">
                        <use xlinkHref="img/sprite.svg#icon-arrow"></use>
                      </svg>
                    </span>
                  </div>
                </a>
              </div>
              <div className="contacts__item">
                <a
                  className="contacts__link link-effect"
                  href="https://www.instagram.com/cheatcode.simf/"
                  target="_blank"
                >
                  <div>
                    <div className="contacts__item-title">
                      <svg className="icon icon-inst ">
                        <use xlinkHref="img/sprite.svg#icon-inst"></use>
                      </svg>
                      <span>Instagram</span>
                    </div>
                    <div className="contacts__item-info">
                      <p>cheatcode.simf</p>
                    </div>
                  </div>
                  <div className="contacts__link-arrow">
                    <span className="btn btn--circle">
                      <svg className="icon icon-arrow ">
                        <use xlinkHref="img/sprite.svg#icon-arrow"></use>
                      </svg>
                    </span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}
