import Head from "next/head";
import Link from "next/link";

export default function Feedback() {
  return (
    <>
      <Head>
        <title>Обратная связь</title>
      </Head>
      <main className="start-page">
        <div className="start-page__wrap feedback-page__wrap">
          <Link prefetch={false} className="logo link-effect" href="/">
            <img src="/img/logo.png" alt="" />
          </Link>
          <strong className="feedback__main-text">
            Напишите нам, если у вас возник вопрос или пожелания по улучшению
            нашего заведения
          </strong>
          <div className="feedback__actions">
            <div className="blur-bg">
              <Link
                prefetch={false}
                className="btn btn--size-m btn--blur-bg"
                href="https://forms.gle/ct5FuHDG8P4uhZBe9"
              >
                <span>Оставить отзыв</span>
              </Link>
            </div>
            <p>Google форма</p>
          </div>
        </div>
      </main>
    </>
  );
}
