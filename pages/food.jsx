import { useEffect, useState } from "react";

import ContentTabs from "@/components/ContentTabs";
import TopMenu from "@/components/TopMenu";
import { fetchFoodsList } from "@/services/foodsList";
import { fetchIconsList } from "@/services/iconsList";
import Head from "next/head";

export async function getServerSideProps() {
  const { foodList, foodListTypes } = await fetchFoodsList();
  const { iconsList } = await fetchIconsList();

  return {
    props: {
      foodList,
      foodListTypes,
      iconsList,
    },
  };
}

export default function Food({ foodList, foodListTypes, iconsList }) {
  const [currentType, setCurrentType] = useState();

  useEffect(() => {
    setCurrentType(foodListTypes[0]);
  }, []);
  
  useEffect(() => {
    window.scrollTo({ top: 0 });
  }, [currentType]);

  return (
    <>
      <Head>
        <title>Еда</title>
      </Head>
      <main>
        <TopMenu
          iconsList={iconsList}
          types={foodListTypes}
          currentType={currentType}
          setCurrentType={setCurrentType}
        />
        <ContentTabs
          contentName={currentType}
          contentData={foodList[currentType]}
        />
      </main>
    </>
  );
}
