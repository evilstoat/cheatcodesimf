import { useEffect, useState } from "react";

import ContentTabs from "@/components/ContentTabs";
import TopMenu from "@/components/TopMenu";

import { fetchDrinksList } from "@/services/drinksList";
import { fetchIconsList } from "@/services/iconsList";
import Head from "next/head";

export async function getServerSideProps() {
  const { drinkList, drinkListTypes } = await fetchDrinksList();
  const { iconsList } = await fetchIconsList();

  return {
    props: {
      drinkList,
      drinkListTypes,
      iconsList,
    },
  };
}

export default function Drinks({ drinkList, drinkListTypes, iconsList }) {
  const [currentType, setCurrentType] = useState();

  useEffect(() => {
    setCurrentType(drinkListTypes[0]);
  }, []);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }, [currentType]);

  return (
    <main>
      <Head>
        <title>Бар</title>
      </Head>
      <TopMenu
        iconsList={iconsList}
        types={drinkListTypes}
        currentType={currentType}
        setCurrentType={setCurrentType}
      />
      <ContentTabs contentName={currentType} contentData={drinkList[currentType]} />
    </main>
  );
}
