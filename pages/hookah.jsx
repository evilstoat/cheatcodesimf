import { fetchhookahsList } from "@/services/hookahList";
import Head from "next/head";
import Link from "next/link";
import React from "react";

export async function getServerSideProps() {
  const { hookahList } = await fetchhookahsList();

  return {
    props: {
      hookahList
    },
  };
}

export default function Hookah({ hookahList }) {
  const renderHookahElement = ({ title, price }) => (
    <React.Fragment key={title}>
      <p>{title}</p>
      <strong>{price}</strong>
    </React.Fragment>
  )

  return (
    <>
      <Head>
        <title>Кальяны</title>
      </Head>
      <main className="start-page start-page--animated">
        <div className="start-page__wrap hookah-page__wrap">
          <Link prefetch={false} className="logo link-effect" href="/">
            <img src="./img/logo.png" alt="" />
          </Link>
          <div className="hookah__info hookah__top">
            <h2>Дорогой гость!</h2>
            <p>
              У нас ты сможешь попробовать более 500 позиций различных вкусов,
              собранных со всего мира. Если дымить, то в правильном месте!
            </p>
          </div>
          <div className="hookah__info hookah__bottom">
            <h3>Временные промежутки:</h3>
            {
              hookahList.map(hookahElement => renderHookahElement(hookahElement))
            }
          </div>
        </div>
      </main>
    </>
  );
}
