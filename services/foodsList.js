import { fetchGoogleSheets } from "@/src/lib/fetchGoogleSheets";
import { convertSheetsToTable, extractTypes, groupByType } from "@/src/lib/utils";

export const fetchFoodsList = async () => {
  const foodListRaw = await fetchGoogleSheets(`Еда!A${3}:F`);

  const schema = [
    "name",
    "type",
    "price",
    "volume",
    "description",
    "imageLink",
  ];

  const normalizedFoodList = convertSheetsToTable(foodListRaw, schema);

  const foodListTypes = extractTypes(normalizedFoodList);

  const foodList = groupByType(normalizedFoodList);

  return {
    foodList,
    foodListTypes,
  };
};
