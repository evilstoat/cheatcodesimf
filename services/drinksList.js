import { fetchGoogleSheets } from "@/src/lib/fetchGoogleSheets";
import { convertSheetsToTable, extractTypes, groupByType } from "@/src/lib/utils";

export const fetchDrinksList = async () => {
  const drinkListRaw = await fetchGoogleSheets(`Напитки!A${3}:H`);

  const schema = [
    "name",
    "type",
    "subtype",
    "price",
    "volume",
    "description",
    "imageLink"
  ];

  const normalizedDrinkList = convertSheetsToTable(drinkListRaw, schema);

  const drinkListTypes = extractTypes(normalizedDrinkList);

  const drinkList = groupByType(normalizedDrinkList);

  return {
    drinkList,
    drinkListTypes,
  };
};
