import { fetchGoogleSheets } from "@/src/lib/fetchGoogleSheets";
import { convertSheetsToTable } from "@/src/lib/utils";

export const fetchhookahsList = async () => {
  const hookahListRaw = await fetchGoogleSheets(`Кальяны!A${3}:B`);

  const schema = [
    "title",
    "price"
  ];

  const hookahList = convertSheetsToTable(hookahListRaw, schema);

  return {
    hookahList
  };
};
