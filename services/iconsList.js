import { fetchGoogleSheets } from "@/src/lib/fetchGoogleSheets";
import { convertSheetsToTable } from "@/src/lib/utils";

export const fetchIconsList = async () => {
  const iconsListRaw = await fetchGoogleSheets(`Иконки!A${3}:B`);

  const schema = ["name", "icon"];

  const iconsList = convertSheetsToTable(iconsListRaw, schema);

  return {
    iconsList
  };
};
