export const convertSheetsToTable = (data, schema = ['titles', 'type', 'subtype', 'volumes', 'prices', 'priceExtra', 'descriptions', 'photo'], entriesToSplit = ['titles', 'volumes', 'prices', 'descriptions']) => {
  return data.reduce((acc, cur) => {
    let entryToPush = {}

    schema.forEach((schemaEntryName, index) => {
      if (entriesToSplit.includes(schemaEntryName)) {
        
        try {
          const splittedEntry = cur[index].split('; ')

          if (!splittedEntry || !splittedEntry[0]) {
            entryToPush[schemaEntryName] = null
          } else {
            entryToPush[schemaEntryName] = cur[index].split('; ') || null
          }
        } catch (_) {
          entryToPush[schemaEntryName] = []
        }


        return
      }

      entryToPush[schemaEntryName] = cur[index] || null
    })

    acc.push(entryToPush)

    return acc
  }, [])
}

export const groupByType = (data) => {
  return data.reduce((acc, cur) => {
    // Check if the current item has 'type'
    if (!acc[cur.type]) {
      acc[cur.type] = {};
    }

    // Check if the current item has 'subtype'
    if (cur.subtype) {
      // If subtype does not exist, create an object and array for it
      if (!acc[cur.type][cur.subtype]) {
        acc[cur.type][cur.subtype] = [];
      }
      // Push the current item into the array of its subtype
      acc[cur.type][cur.subtype].push(cur);
    } else {
      // If 'subtype' doesn't exist, use a default key to store items without subtypes
      // Ensure an array for items without subtypes exists
      if (!acc[cur.type]['default']) {
        acc[cur.type]['default'] = [];
      }
      // Push the current item into the 'default' array
      acc[cur.type]['default'].push(cur);
    }

    return acc;
  }, {});
}

export const extractTypes = (data) => {
  const firstPriorityTypes = ['Летнее предложение', 'Осеннее предложение', 'Зимнее предложение', 'Весеннее предложение'];

  const result = new Set();

  data.forEach(({ type }) => result.add(type));

  let resultArray = Array.from(result);

  const priorityItems = firstPriorityTypes.filter(type => resultArray.includes(type));
  resultArray = resultArray.filter(type => !firstPriorityTypes.includes(type));
  resultArray = [...priorityItems, ...resultArray];

  return resultArray;
}
