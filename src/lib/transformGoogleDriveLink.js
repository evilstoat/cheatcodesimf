export function transformGoogleDriveLink(driveUrl) {
  const fileIdMatch = driveUrl.match(/\/d\/(.*?)\//);
  
  if (!fileIdMatch || fileIdMatch.length < 2) {
      throw new Error('Invalid Google Drive URL');
  }
  
  const fileId = fileIdMatch[1];
  
  const googleusercontentUrl = `https://lh3.googleusercontent.com/d/${fileId}?sz=w1000`;
  
  return googleusercontentUrl;
}